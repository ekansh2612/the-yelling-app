import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class YellingTest {

	
	Yelling person;
	String expectedOutput;
	@Before
	public void setUp() throws Exception {
		person = new Yelling();
		
	}
	
	
	@Test
	
	// R1 - One person is yelling
	
	
	public void test1Yelling() {
		
		expectedOutput = "Peter is yelling";
		String input[] = { "Peter" };
		assertEquals(expectedOutput, person.scream(input));
		
	} 
	
	// R2 - Nobody is yelling
	// If we put null we should get Nobody is yelling
	
	@Test
	public void testNullYelling() {
		
		expectedOutput = "Nobody is yelling";
		String input[] = { null }  ;
		assertEquals(expectedOutput, person.scream(input));
	} 
	
	// R3 - All Uppercase letters
	
	@Test
	public void testUCYelling() {
		
		expectedOutput = "PETER IS YELLING";
		String input[] = { "PETER" };
		assertEquals(expectedOutput, person.scream(input));
	}
	
	
	// R4 - Two People are yelling
	
	@Test
	public void test2Yelling() {
		
		String input[] = { "Peter","Kadeem"} ; 
		expectedOutput = "Peter and Kadeem are yelling";
		assertEquals(expectedOutput, person.scream(input));
	
	} 
	
	// R5 - More than two people are yelling
	
	@Test
	public void testMoreThan2Yelling() { 
		
		String input[] = { "Peter","Kadeem","Albert"};
		expectedOutput = "Peter, Kadeem, and Albert are yelling";
		assertEquals(expectedOutput, person.scream(input));
		
	}
	
	// R6 -  Shouting at a lot of people
	
	@Test
	public void testJumbledYelling() {
		
		String input[] = { "Peter","EMAD", "Albert", "Kadeem" };
		expectedOutput = "Albert, Kadeem, and Peter are yelling, SO IS EMAD";
		assertEquals(expectedOutput, person.scream(input));
	}

}


