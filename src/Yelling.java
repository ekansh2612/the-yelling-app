import java.util.Arrays;


public class Yelling {
	

	public Object scream(String[] name) 
	{
		String newInput = Arrays.toString(name).replace("]", "").replace("[",""); 	// For removing '[]'
		
		
		String output = newInput + " " + "is yelling";
		
		if (newInput.equals(newInput.toLowerCase()))
		{ 
			
			output = "Nobody is yelling";
			
		}
		
		
		else if (newInput.equals(newInput.toUpperCase()))    // Checking for Uppercase letters
		{ 
			output = output.toUpperCase(); 
			
		}
		
		
		else if (name.length == 2)
		{
			
			newInput = String.join(" and ", name);  		// Adding 'and' between names
			output = newInput + " " + "are yelling";
		}
		
		
		else if (name.length > 2 && name.length < 4)
		{
			
			String lastName = name[name.length-1];          	// Copying last element
			String[] name2 = Arrays.copyOf(name, name.length-1);   		// Removing last element
			newInput = Arrays.toString(name2).replace("]", "").replace("[","");    		// Removing []
			newInput = String.join(", ", name2);
			output = newInput + ", and " + lastName + " " + "are yelling";
			
		}
		
		
		else if (name.length > 3 )
		{
			
			String uppercaseName = "";      
			for( int i = 0; i < name.length; i++)
			{ 
				
				if(name[i].equals(name[i].toUpperCase()));  // finding uppercase name
				{
					
					uppercaseName = name[i];
					
				
					// Removing Uppercase Name from the array
				for ( int j = i + 1 ; j < name.length; j++)
					{
					     name[j-1] = name[j];
					 } 
			//	System.out.println(name[i]);
				}
			}
			
			String lastName = name[name.length-1];          	// Copying last element
			String[] name2 = Arrays.copyOf(name, name.length-1);   		// Removing last element
			newInput = Arrays.toString(name2).replace("]", "").replace("[","");    		// Removing []
			newInput = String.join(", ", name2);
			output = newInput + ", and " + lastName + " " + "are yelling" + ", " + "SO IS " + uppercaseName;
		}

		
		return output;
	}

	

}


